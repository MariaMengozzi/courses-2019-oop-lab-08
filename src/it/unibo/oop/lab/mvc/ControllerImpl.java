package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/**
 * 
 */
public class ControllerImpl implements Controller {

    private String nextString = new String();
    private List<String> stringHistory = new ArrayList<>();

    @Override
    public final void setNextStringToPrint(final String string) {
        this.nextString = Objects.requireNonNull(string, "This method does not accept null values.");
    }

    @Override
    public final String getNextStringToPrint() {
        return this.nextString;
    }

    @Override
    public final List<String> getHistoryOfPrintedString() {
        return this.stringHistory;
    }

    @Override
    public final void printCurrentString() {
        if (this.nextString == null) {
            throw new IllegalStateException("There is no string set");
        }
        stringHistory.add(this.nextString);
        System.out.println(this.nextString);
    }
}
